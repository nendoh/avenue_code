Feature: Create SubTask
  As a ToDo App user
  I should be able to create a sub-task
  So I can break down my tasks in smaller pieces

  Background:
    Given I am logged in as "noahkidding@avenuecode.com" with password "noahkidding"
    And I navigate to the ToDo list page

  Scenario: 1. The user should see a button labeled as "Manage Subtasks".
    Given I delete all the memos
    And I add a random UUID task and hit the plus button
    Then I should see a Manage Subtasks button next to the UUID task.

  Scenario: 2. The Manage Subtasks button should have the number of subtasks created for that tasks.
    Given I add a random UUID task and hit the plus button
    And I click the Manage Subtasks button next to the UUID task.
    And I type "Sub-task 1" in the SubTask Description: field of the Editing Task dialog
    And I click the +Add button
    And I click the Close button.
    Then I should see that the (0) Manage Subtasks button has changed to (1) Manage Subtasks.

  Scenario: 3. The popup modal dialog should have a read-only field with the task ID and the task description.
    Given I delete all the memos
    And I add a random UUID task and hit the plus button
    And I click the Manage Subtasks button next to the UUID task.
    Then I should not be able to edit the text in the task description.

  Scenario: 4. The SubTask Description should not accept 254 characters.
    Given I delete all the memos
    And I add a random UUID task and hit enter
    And I click the Manage Subtasks button next to the UUID task.
    And I type 254 characters in the SubTask Description field and click the '+ Add' button
    Then The text should be truncated to 250 characters.

  Scenario: 5. The SubTask Description should accept 250 characters.
    Given I delete all the memos
    And I add a random UUID task and hit enter
    And I click the Manage Subtasks button next to the UUID task.
    And I type 250 characters in the SubTask Description field and click the '+ Add' button
    Then The text should contain 250 characters.

  Scenario: 6. The Task Description is a required field - part 1.
    Given I type "Task 7" in the "Type a new task here and press enter" field
    And I hit the enter key
    And I click the "(0) Manage Subtasks" button next to Task 7.
    And I place the cursor in the "Type a new SubTask here and press enter" field.
    And I enter no text in the "Type a new SubTask here and press enter" field.
    And I hit the enter key.
    Then I should NOT see a new entry in the list of "SubTasks of this ToDo:" section.

  Scenario: 7. The Task Description is a required field - part 2.
    Given I type "Task 8" in the "Type a new task here and press enter" field
    And I hit the enter key
    And I click the "(0) Manage Subtasks" button next to Task 8.
    And I click the '+ Add' button.
    Then I should NOT see a new entry in the list of "SubTasks of this ToDo:" section.

  Scenario: 8. The Due Date is a required field - part 1.
    Given I type "Task 9" in the "Type a new task here and press enter" field
    And I hit the enter key
    And I click the "(0) Manage Subtasks" button next to Task 9.
    And I clear the text from the "Due Date:" field.
    And I click the '+ Add' button.
    Then I should NOT see a new entry in the list of "SubTasks of this ToDo:" section.

  Scenario: 9. The Due Date is a required field - part 2.
    Given I type "Task 10" in the "Type a new task here and press enter" field
    And I hit the enter key
    And I click the "(0) Manage Subtasks" button next to Task 10.
    And I clear the text from the "Due Date:" field.
    And I hit the enter key.
    Then I should NOT see a new entry in the list of "SubTasks of this ToDo:" section.