Feature: Create Task
  As a ToDo App user
  I should be able to create a task So I can manage my tasks

  Scenario: 1. The user should see the ‘My Tasks’ link on the NavBar even when not logged in.
    Given I am on the avenuecode site
    And I am not logged in
    Then I should see a link that says "My Tasks"

  Scenario: 2. The user should see the ‘My Tasks’ link on the NavBar after logging in.
    Given I am on the avenuecode site
    And I am not logged in
    When I log in as "noahkidding@avenuecode.com" with password "noahkidding"
    Then I am logged in
    And I should see a link that says "My Tasks"

  Scenario: 3. The user should see a message on the top part saying that list belongs to the logged user.
    Given I am logged in as "noahkidding@avenuecode.com" with password "noahkidding"
    And I navigate to the ToDo list page
    Then I should see "Hey Noah, this is your todo list for today:"

  Scenario: 4. The user should be able to enter a new task by clicking the add task button.
    Given I am logged in as "noahkidding@avenuecode.com" with password "noahkidding"
    And I navigate to the ToDo list page
    And I add a random UUID task and hit enter
    Then I should see the UUID in the list of tasks.

  Scenario: 5. The user should be able to enter a new task by clicking the add task button.
    Given I am logged in as "noahkidding@avenuecode.com" with password "noahkidding"
    And I navigate to the ToDo list page
    And I add a random UUID task and hit the plus button
    Then I should see the UUID in the list of tasks.

  Scenario: 6. The task should require at least three characters.
    Given I am logged in as "noahkidding@avenuecode.com" with password "noahkidding"
    And I navigate to the ToDo list page
    And I type "01" in the New Task field and hit the plus button
    Then I should not see "01" in the list of tasks.

  Scenario: 7. The task should not accept an empty string.
    Given I am logged in as "noahkidding@avenuecode.com" with password "noahkidding"
    And I navigate to the ToDo list page
    And I type 0 characters in the New Task field and hit plus
    Then I should not see "empty" in the list of tasks.

  Scenario: 8. The task can't have more than 250 characters.
    Given I am logged in as "noahkidding@avenuecode.com" with password "noahkidding"
    And I navigate to the ToDo list page
    And I type 254 characters in the New Task field and hit plus
    Then I should NOT see 254 characters in the list of tasks.