class TestStrings

  def avenue_code_url
    return "http://qa-test.avenuecode.com/" # "http://www.clayjackson.com/" #
  end

  def get_long_string(num_characters)
    length = num_characters.to_i
    text_string = nil
    case length
      when 256
        text_string = "aaaaaaaaaaaaaaaaaaaaaaaThirtyTwoaaaaaaaaaaaaaaaaaaaaaaaSixtyFouraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbOneHundredTwentyEightbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccccccccccccccccccTwoHundredFiftySix"
      when 255
        text_string = "aaaaaaaaaaaaaaaaaaaaaaaThirtyTwoaaaaaaaaaaaaaaaaaaaaaaaSixtyFouraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbOneHundredTwentyEightbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccccccccccccccccccTwoHundredFiftySi"
      when 254
        text_string = "aaaaaaaaaaaaaaaaaaaaaaaThirtyTwoaaaaaaaaaaaaaaaaaaaaaaaSixtyFouraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbOneHundredTwentyEightbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccccccccccccccccccTwoHundredFiftyS"
      when 250
        text_string = "aaaaaaaaaaaaaaaaaaaaaaaThirtyTwoaaaaaaaaaaaaaaaaaaaaaaaSixtyFouraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbOneHundredTwentyEightbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccccccccccccccccccTwoHundredFi"
      when 32
        text_string = "aaaaaaaaaaaaaaaaaaaaaaaThirtyTwo"
    end
    return text_string
  end # get_long_string(num_characters)

end