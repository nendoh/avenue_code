class SubTaskPage

  include PageObject

  text_area(:to_do, :id => "edit_task")
  text_field(:description, :id => "new_sub_task")
  text_field(:due_date, :id => "dueDate")

  button(:add_subtask, :id => "add-subtask")
  button(:close, :css => "button.btn-primary")

  link(:subtask_link_254, :link_text => "aaaaaaaaaaaaaaaaaaaaaaaThirtyTwoaaaaaaaaaaaaaaaaaaaaaaaSixtyFouraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbOneHundredTwentyEightbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccccccccccccccccccTwoHundredFiftyS")
  link(:subtask_link_250, :link_text => "aaaaaaaaaaaaaaaaaaaaaaaThirtyTwoaaaaaaaaaaaaaaaaaaaaaaaSixtyFouraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbOneHundredTwentyEightbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccccccccccccccccccTwoHundredFi")
  link(:subtask_link_32,  :link_text => "aaaaaaaaaaaaaaaaaaaaaaaThirtyTwo")
  def click_outside_dialog
    begin
      button = @browser.find_element(:css, "button.btn-primary")
      @browser.action.move_to(button).move_by(150, 150).click.perform
    rescue
      puts "Couldn't click outside dialog"
    end
  end

  def click_subtask_link
    elements = @browser.find_elements(:css, "a.ng-binding")
    elements[0].click
  end
end