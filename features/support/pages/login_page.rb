class LoginPage

  include PageObject

  text_field(:login_name, :id => "user_email")
  text_field(:login_password, :id => "user_password")
  button(:sign_in, :class => "btn-primary")

  def sign_in_as(login_name, login_password)
    self.login_name = login_name
    self.login_password = login_password
    self.sign_in
  end

end