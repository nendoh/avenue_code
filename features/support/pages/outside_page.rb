class OutsidePage

  include PageObject

  link(:sign_in, :href => "/users/sign_in")
  link(:my_tasks, :link_text => "My Tasks")

  def find_link(link_text)
    begin
      link = @browser.find_element(:link_text, link_text)
      found = true
    rescue
      found = false
    end
    return found
  end

end