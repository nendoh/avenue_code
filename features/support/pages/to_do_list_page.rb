class ToDoListPage

  include PageObject
  require 'securerandom'

  text_field(:new_task, :id => "new_task")

  link(:sign_in, :href => "/users/sign_in")
  link(:my_tasks, :link_text => "My Tasks")

  def button_text_for(task_text)
    button = self.find_button_for(task_text)
    return button.text
  end

  def create_new_task(task_text)
    self.new_task = task_text
  end

  def create_new_task_and_hit_enter(task_text)
    self.new_task = task_text + "\n"
  end

  def create_new_task_and_hit_plus(task_text)
    self.new_task = task_text
    self.hit_plus_button
  end

  def create_random_uuid_task_and_hit_enter
    @random_string = SecureRandom.uuid
    self.new_task = @random_string + "\n"
  end

  def create_random_uuid_and_hit_plus
    @random_string = SecureRandom.uuid
    self.new_task = @random_string
    self.hit_plus_button
  end

  def compare_task_count
    sleep 1
    compare_to = @task_count.to_i
    print "IN compare_task_count, compare_to: " + compare_to + "\n"
    return compare_to - self.count_tasks
  end

  def count_tasks
    sleep 1
    @task_count = 0
    begin
      elements = @browser.find_elements(:css, "a.editable-click")
      elements.each { |tag|
        @task_count += 1
      }
    rescue
      puts "ERROR: could not locate \"new task\" field"
    end
    print "IN count_tasks, @task_count: " + @task_count.to_s + "\n"
    return @task_count
  end


  def find_button_for(task_text)
    button = nil
    found = false
    begin
      # first we find the task we're interested in...
      task_index = 0
      button_index = -1
      elements = @browser.find_elements(:css, "a.editable-click")
      elements.each { |tag|
        if tag.text == task_text
          button_index = task_index
        else
          task_index += 1
        end
      }

      begin
        # ...then we find the button we're interested in
        elements = @browser.find_elements(:css, "button.btn-primary")
        button = elements[button_index]

      rescue
        puts "ERROR: could not locate \"Manage Subtasks\" button"
      end
    rescue
      puts "ERROR: could not locate task in list"
    end
    return button
  end # find_button_for(task_text)

  def find_heading_with_text(heading_text)
    begin
      elements = @browser.find_elements(:tag_name, "h1")
      found = false
      elements.each { |tag|
        if tag.text == heading_text
          found = true
        end
      }
    rescue
      puts "ERROR: no h1 tags found on page"
    end
    return found
  end # find_heading_with_text(heading_text)

  def find_link(link_text)
    begin
      link = @browser.find_element(:link_text, link_text)
      found = true
    rescue
      found = false
    end
    return found
  end

  def hit_plus_button
    button = @browser.find_element(:css, "span.input-group-addon")
    sleep 1
    button.click
  end

  def random_uuid
    return @random_string
  end

  def remove_tasks
    begin
      button = @browser.find_element(:css, "button.btn-danger")
      while button != nil
        button.click
        self.my_tasks
        sleep 1
        button = @browser.find_element(:css, "button.btn-danger")
      end
    rescue

    end
  end # remove_tasks
end