require_relative '../support/includes/test_strings.rb'

def enter_long_subtask_and_hit_add(num_characters)
  test_strings = TestStrings.new()
  @subTaskPage = SubTaskPage.new(@browser)
  @subTaskPage.description = test_strings.get_long_string(num_characters)
  @subTaskPage.add_subtask
end # enter_long_subtask_and_hit_add(num_characters)

Given(/^I type "([^"]*)" in the SubTask Description: field of the Editing Task dialog$/) do |subtask_text|
  @subTaskPage = SubTaskPage.new(@browser)
  @subTaskPage.description = subtask_text
end

Given(/^I click the \+Add button$/) do
  @subTaskPage.add_subtask
end

Given(/^I click the Close button\.$/) do
  @subTaskPage.close
end

Then(/^I should see that the \((\d+)\) Manage Subtasks button has changed to \((\d+)\) Manage Subtasks\.$/) do |previous_count, count|
  text = @toDoListPage.button_text_for(@toDoListPage.random_uuid)
  expect(text).to eql "(" + count + ") Manage Subtasks"
end

Then(/^I should not be able to edit the text in the task description\.$/) do
  @subTaskPage = SubTaskPage.new(@browser)
  before_text = @subTaskPage.to_do
  @subTaskPage.to_do = "Text area is not read-only"
  after_text = @subTaskPage.to_do
  @subTaskPage.close
  @toDoListPage.my_tasks
  expect(after_text).to eql(before_text)
end

Given(/^I click outside the dialog\.$/) do
  SubTaskPage.new(@browser).click_outside_dialog
end

Given(/^I type (\d+) characters in the SubTask Description field and click the '\+ Add' button$/) do |num_characters|
  enter_long_subtask_and_hit_add(num_characters)
end

Given(/^I click the text link in the SubTasks of this ToDo section\.$/) do
  @subTaskPage.click_subtask_link
end

Then(/^The text should be truncated to (\d+) characters\.$/) do |num_characters|
  link_length = 250
  if num_characters.to_i > 250
    @subTaskPage = SubTaskPage.new(@browser)
    begin
      link_text = @subTaskPage.subtask_link_254_element

      print "link_text: " + link_text.text + "\n"

      print "ERROR: link_text.length == " + link_text.length + ", should be 250 or less\n"
      link_length = link_text.length
    rescue
      link_length = -1
    end
  end
  expect(link_length).to eql(250)
end

Then(/^The text should contain (\d+) characters\.$/) do |num_characters|
  link_length = 0
  if num_characters.to_i == 250
    @subTaskPage = SubTaskPage.new(@browser)
    begin
      link_text = @subTaskPage.subtask_link_250_element
      print "SUCCESS: link_text.length == " + link_text.length + ", should be 250 or less\n"
      link_length = link_text.length
    rescue
      link_length = -1
    end
  end
  expect(link_length).to eql(250)
end

Then(/^I should NOT see a "([^"]*)" button next to the task\.$/) do |arg1|
  puts "TODO: Then(/^I should NOT see a \"([^\"]*)\" button next to the task\.$/) "
end

Given(/^I place the cursor in the "([^"]*)" field\.$/) do |arg1|
  puts "TODO: Given(/^I place the cursor in the \"([^\"]*)\" field\.$/) "
end

Given(/^I enter no text in the "([^"]*)" field\.$/) do |arg1|
  puts "TODO: Given(/^I enter no text in the \"([^\"]*)\" field\.$/) "
end

Given(/^I hit the enter key\.$/) do
  puts "TODO: Given(/^I hit the enter key\.$/) "
end

Then(/^I should NOT see a new entry in the list of "([^"]*)" section\.$/) do |arg1|
  puts "TODO: Then(/^I should NOT see a new entry in the list of \"([^\"]*)\" section\.$/)  "
end

Given(/^I clear the text from the "([^"]*)" field\.$/) do |arg1|
  puts "TODO: Given(/^I clear the text from the \"([^\"]*)\" field\.$/) "
end
