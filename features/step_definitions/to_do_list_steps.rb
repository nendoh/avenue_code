require_relative '../support/includes/test_strings.rb'

def count_tasks
  @toDoListPage.count_tasks
end

def count_unchanged(unchanged)
  difference = @toDoListPage.compare_task_count
  if unchanged == true
    expect(difference).to eql 0
  else
    expect(difference).not_to eql 0
  end
end

def enter_long_task_and_hit_plus(num_characters)
  test_strings = TestStrings.new()
  task_text = test_strings.get_long_string(num_characters)
  @toDoListPage = ToDoListPage.new(@browser)
  @toDoListPage.my_tasks
  @toDoListPage.create_new_task_and_hit_plus(task_text)
end # enter_long_task_and_hit_plus(num_characters)

def find_task_in_list(task_text)
  found = found_in_list(task_text)
  expect(found).to eql true
end

def found_in_list(task_text)
  sleep 1
  found = false
  begin
    elements = @browser.find_elements(:css, "a.editable-click")
    elements.each { |tag|
      if tag.text == task_text
        found = true
      end
    }
  rescue
    puts "ERROR: could not locate \"new task\" field"
  end
  return found
end

def task_should_not_be_in_list(task_text)
  found = found_in_list(task_text)
  expect(found).to eql false
end

Given(/^I am logged in$/) do
  @toDoListPage = ToDoListPage.new(@browser)
  expect(@toDoListPage.find_link("Sign out")).to eql true
  expect(@toDoListPage.find_link("Sign In")).to eql false
end

Given(/^I am logged in as "([^"]*)" with password "([^"]*)"$/) do |login_name, login_password|

  testStrings = TestStrings.new
  @browser.get testStrings.avenue_code_url()

  @toDoListPage = ToDoListPage.new(@browser)
  @toDoListPage.sign_in

  @loginPage = LoginPage.new(@browser)
  @loginPage.sign_in_as(login_name, login_password)

  expect(@toDoListPage.find_link("Sign out")).to eql true
  expect(@toDoListPage.find_link("Sign In")).to eql false
end

Given(/^I navigate to the ToDo list page$/) do
  @toDoListPage = ToDoListPage.new(@browser)
  @toDoListPage.my_tasks
end

Then(/^I should see "([^"]*)"$/) do |heading_text|
  expect(@toDoListPage.find_heading_with_text(heading_text)).to eql true
end

Given(/^I type "([^"]*)" in the New Task field$/) do |task_text|
  @toDoListPage.create_new_task(task_text)
end

Given(/^I add a random UUID task and hit enter$/) do
  @toDoListPage.create_random_uuid_task_and_hit_enter
end

Given(/^I type "([^"]*)" in the New Task field and hit enter$/) do |task_text|
  @toDoListPage.create_new_task_and_hit_enter(task_text)
end

Then(/^I should see "([^"]*)" in the list of tasks\.$/) do |task_text|
  find_task_in_list(task_text)
end

Given(/^I type "([^"]*)" in the New Task field and hit the plus button$/) do |task_text|
  @toDoListPage.my_tasks
  @toDoListPage.create_new_task_and_hit_plus(task_text)
end

Given(/^I add a random UUID task and hit the plus button$/) do
  @toDoListPage.create_random_uuid_and_hit_plus
end

Then(/^I should see the UUID in the list of tasks\.$/) do
  @toDoListPage.my_tasks
  find_task_in_list(@toDoListPage.random_uuid)
end

Then(/^I should not see "([^"]*)" in the list of tasks\.$/) do |task_text|
  task_should_not_be_in_list(task_text)
end

Given(/^I count the number of tasks$/) do
  @toDoListPage.my_tasks
  count_tasks
end

Given(/^I type (\d+) characters in the New Task field and hit plus$/) do |num_characters|
  @toDoListPage.my_tasks
  enter_long_task_and_hit_plus(num_characters)
end

Then(/^the number of tasks should be unchanged\.$/) do
  @toDoListPage.my_tasks
  count_unchanged(true)
end

Then(/^I should NOT see (\d+) characters in the list of tasks\.$/) do |num_characters|
  test_strings = TestStrings.new()
  task_text = test_strings.get_long_string(num_characters)
  task_should_not_be_in_list(task_text)
end

### BEGIN SubTask stuff

def delete_tasks
  begin
    elements = @browser.find_elements(:css, "button.btn-danger")
    num_elements = elements.count

    while num_elements > 0
      elements[0].click
      @toDoListPage.my_tasks
      sleep 1
      elements = @browser.find_elements(:css, "button.btn-danger")
      num_elements = elements.count
    end
  rescue
    puts "ERROR in delete_tasks: could not locate Remove button"
  end
end

def click_manage_subtask_for(task_text)
#  button = find_manage_subtask_button_for(task_text)
  button = @toDoListPage.find_button_for(task_text)
  if button != nil
    button.click
  end
end

Given(/^I delete all the memos$/) do
  @toDoListPage.remove_tasks
end

Then(/^I should see a Manage Subtasks button next to the UUID task\.$/) do
  expect(@toDoListPage.find_button_for(@toDoListPage.random_uuid)).not_to be nil
#  expect(find_manage_subtask_button_for("nada")).not_to be nil
#puts  find_manage_subtask_button_for("nada").attribute("innerHTML")
end

Given(/^I click the Manage Subtasks button next to the UUID task\.$/) do
  click_manage_subtask_for(@toDoListPage.random_uuid)
end