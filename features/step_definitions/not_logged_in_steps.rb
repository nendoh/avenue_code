require_relative '../support/includes/test_strings.rb'

Given(/^I am on the avenuecode site$/) do
  testStrings = TestStrings.new
  @browser.get testStrings.avenue_code_url()
end

Given(/^I am not logged in$/) do
  @toDoListPage = ToDoListPage.new(@browser)
  expect(@toDoListPage.find_link("Sign In")).to eql true
  expect(@toDoListPage.find_link("Sign out")).to eql false
end

Then(/^I should see a link that says "([^"]*)"$/) do |link_text|
  @toDoListPage = ToDoListPage.new(@browser)
  expect(@toDoListPage.find_link(link_text)).to eql true
end

When(/^I log in as "([^"]*)" with password "([^"]*)"$/) do |login_name, login_password|
  @toDoListPage = ToDoListPage.new(@browser)
  @toDoListPage.sign_in
  sleep 1
  @loginPage = LoginPage.new(@browser)
  @loginPage.sign_in_as(login_name, login_password)
end